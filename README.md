# Guía de estilo general

## Declaración de variables

Utilizar las palabras reservadas **let** y **const**, en lugar de **var**, a menos que sea estríctamente necesario por cuestiones de scope (lo que no ocurre en la mayoría de los casos).


    // incorrecto
    var array = [];
    var edad = 20;
    // correcto
    const array = [];
    let edad = 20


## Sentencias condicionales

Comenzar siempre por las validaciones negativas, para evitar anidaciones innecesarias de scopes.


    // incorrecto
    if(condicion) {
	    //... muchas lineas después
    } else {
	    return 'falso!';
    }
	    
	// correcto
    if(!condicion) {
		   return 'falso!';
    }
    // continúa ejecución del código
	

## Nombres declarativos

A la hora de nombrar variables y funciones, evitar abreviaciones y nombres ambiguos. No es problema que la longitud de los nombres crezca, siempre que sea necesario.

    // incorrecto `enter code here`
    // El nombre de la variable no permite inferir su funcionamiento:
    const s = 'Hola';
    // El nombre de la variable es innecesariamente largo
    const saludoParaRetornar = 'Hola';

    
    // correcto
    const saludo = 'Hola';


## Utilización de comillas simples

Siempre que se trabaje con strings, utilizar comillas simples

    // incorrecto
    const saludo = "Hola";
    
   
    // correcto
    const saludo = 'Hola';